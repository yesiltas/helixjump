﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeableBarrierManager : MonoBehaviour
{
    public List<Renderer> slices = new List<Renderer>();

    public void Initialize(Color barrier)
    {
        foreach (var item in slices)
        {
            item.material.SetColor("_Color", barrier);
        }
    }

    void FixedUpdate()
    {
        transform.rotation = Quaternion.Euler(0, 1 + transform.rotation.eulerAngles.y, 0);
    }
}
