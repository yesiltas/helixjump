﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public enum Status { Wait, Move, }
    public Status status = Status.Wait;

    public float speed;
    public float gravity;
    public float bouncyForce;
    public float maxSpeed;

    public Action PlayerDie;
    public Action PlayerWin;
    public Action<CylinderController, bool> PlayerPassedBarrier;

    Renderer rnd;
    Rigidbody rgb;
    List<int> IDlist = new List<int>();
    bool isEmpowered;
    TrailRenderer trail;

    private void Awake()
    {
        SetPeroperties();
        rgb = GetComponent<Rigidbody>();
        rnd = GetComponent<Renderer>();
        trail = GetComponent<TrailRenderer>();
    }

    public void StartGame()
    {
        SetTrail();
        Vector3 startPosition = GameManager.instance.GetPosition();
        transform.position = startPosition;
        StartCoroutine(ExecuteAfterTime(0.2f, Status.Move));
    }

    public void Again()
    {
        SetPeroperties();
        IDlist.Clear();
    }

    private void SetPeroperties()
    {
        isEmpowered = false;
        speed = 0;
        passedBarrierCount = 0;
    }

    void Move()
    {
        if (speed <= maxSpeed)
            speed += gravity;
        rgb.velocity = Vector3.down * speed * Time.deltaTime;
    }

    private void PlayerCollisionOnTarget()
    {
        speed = bouncyForce;
        ChangeDirection();
        passedBarrierCount = 0;
    }

    private void BreakTarget(CylinderController cylinder, bool isEmpowered)
    {
        PlayerPassedBarrier(cylinder, isEmpowered);
    }

    void ChangeDirection()
    {
        speed = -speed;
    }

    IEnumerator ExecuteAfterTime(float time, Status status)
    {
        yield return new WaitForSeconds(time);
        this.status = status;
    }

    IEnumerator ChangeAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        isEmpowered = false;
    }

    int passedBarrierCount = 0;

    void FixedUpdate()
    {
        switch (status)
        {
            case Status.Wait:
                rgb.velocity = Vector3.zero;
                break;
            case Status.Move:
                Move();
                break;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        float collision_y = collision.transform.position.y;
        if (transform.position.y - collision_y > 0.5f)
        {
            SetTrail();
            if (collision.transform.tag == "Finish")
            {
                if (status == Status.Move)
                {
                    status = Status.Wait;
                    PlayerWin();
                }
            }
            else
            {
                if (passedBarrierCount >= 3)
                {
                    if (collision.transform.tag != "space")
                    {
                        isEmpowered = true;
                        PlayerCollisionOnTarget();
                        BreakTarget(collision.transform.GetComponentInParent<CylinderController>(), isEmpowered);
                        StartCoroutine(ChangeAfterTime(0.01f));
                    }
                }
                else
                {
                    if (!isEmpowered)
                    {
                        if (collision.transform.tag == "barrier")
                        {
                            status = Status.Wait;
                            PlayerDie();
                        }

                        else if (collision.transform.tag == "target")
                        {
                            PlayerCollisionOnTarget();
                        }
                    }
                }
            }
        }
    }

    private void SetTrail()
    {
        trail.time = 0.15f;
        trail.startWidth = 0.2f;
        trail.endWidth = 0.01f;
        trail.material.SetColor("_Color", GameManager.instance.GetTargetColor());
        rnd.material.SetColor("_Color", GameManager.instance.GetTargetColor());
    }

    private void OnTriggerEnter(Collider other)
    {
        CylinderController cylinderController = other.GetComponentInParent<CylinderController>();
        int ID = cylinderController.GetID();
        if (!IDlist.Contains(ID))
        {
            IDlist.Add(ID);
            passedBarrierCount++;
            PlayerPassedBarrier(cylinderController, isEmpowered);
            trail.time += 0.008f;
            trail.startWidth += 0.008f;
            trail.endWidth += 0.0009f;
        }
        if (passedBarrierCount >= 3)
        {
            trail.time = 0.2f;
            trail.startWidth = 0.4f;
            trail.endWidth = 0.05f;
            trail.material.SetColor("_Color", Color.red);
            rnd.material.SetColor("_Color", Color.red);
        }
    }
}

