﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Cylinder
{
    public SpacePoint[] spaces;
    public Barrier[] barriers;
}

[Serializable]
public class SpacePoint
{
    public int spaceFrom;
    public int spaceTo;
}

[Serializable]
public class Barrier
{
    public int barrierFrom;
    public int barrierTo;
}


public class CylinderManager : MonoBehaviour
{
    public GameObject mainCylinder;
    public GameObject cylinder;
    int cylinderCount;
    int levelCylinderCount = 8;
    public Cylinder[] Cylinders;
    MainCylinderManager mainCylinderManager;
    int ID = 1;
    bool isChangeableBarrier = false;

    public void StartGame(bool hardLevel)
    {
        isChangeableBarrier = hardLevel;
        cylinderCount = Cylinders.Length;
        InstantiateMainCylinder();
        Vector3 position = new Vector3(0, (levelCylinderCount * 2) - 2, 0);
        List<int> randomIndex = new List<int>();

        for (int i = 0; i < cylinderCount;)
        {
            int random = UnityEngine.Random.Range(0, cylinderCount);
            if (!(i == 0 && (Cylinders[random].spaces[0].spaceFrom == 0 || Cylinders[random].barriers[0].barrierFrom == 0)))
            {
                if (!randomIndex.Contains(random))
                {
                    randomIndex.Add(random);
                    i++;
                }
            }
        }

        int cylinderIndex = 0;

        for (int i = 0; i < levelCylinderCount; i++)
        {
            List<int> space = new List<int>();
            List<int> barrier = new List<int>();
            for (int j = 0; j < Cylinders[randomIndex[cylinderIndex]].spaces.Length; j++)
            {
                SetList(ref space, Cylinders[randomIndex[cylinderIndex]].spaces[j].spaceFrom, Cylinders[randomIndex[cylinderIndex]].spaces[j].spaceTo);
            }

            for (int j = 0; j < Cylinders[randomIndex[cylinderIndex]].barriers.Length; j++)
            {
                SetList(ref barrier, Cylinders[randomIndex[cylinderIndex]].barriers[j].barrierFrom, Cylinders[randomIndex[cylinderIndex]].barriers[j].barrierTo);
            }
            InstantiateCylinders(position, space, barrier);
            position -= new Vector3(0, 4, 0);
            cylinderIndex++;
            if (cylinderIndex == cylinderCount)
                cylinderIndex = UnityEngine.Random.Range(0, randomIndex.Count);
        }
    }

    public void Again(bool isNextLevel)
    {
        mainCylinderManager.DestroyCylinders();
        Destroy(mainCylinderManager.gameObject);
        ID = 1;
        if (isNextLevel)
            levelCylinderCount += 2;
    }

    public void FinishGame()
    {
        mainCylinderManager.FinishGame();
    }

    private void InstantiateMainCylinder()
    {
        mainCylinderManager = Instantiate(mainCylinder).GetComponent<MainCylinderManager>();
        mainCylinderManager.Initialize(levelCylinderCount * 2);
        mainCylinderManager.StartGame();
    }

    private void InstantiateCylinders(Vector3 position, List<int> space, List<int> barrier)
    {
        CylinderController cylinderController = Instantiate(cylinder).GetComponent<CylinderController>();
        if (isChangeableBarrier && ID % 5 == 0)
            cylinderController.Initialize(position, space, barrier, true);
        else
            cylinderController.Initialize(position, space, barrier, false);
        cylinderController.SetID(ID);
        ID++;
        mainCylinderManager.GetChildCylinder(cylinderController);
    }

    private void SetList(ref List<int> temp, int from, int to)
    {
        for (int i = from; i < to; i += 10)
        {
            temp.Add((int)(i * 0.1f));
        }
    }

    public Vector3 GetPosition()
    {
        return new Vector3(0, levelCylinderCount * 2 + 1, -2f);
    }

    public void RemoveInList(CylinderController cylinder)
    {
        mainCylinderManager.RemoveInList(cylinder);
    }
}

