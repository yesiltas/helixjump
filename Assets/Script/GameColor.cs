﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class LevelColor
{
    public Color backgroundColor;
    public Color targetColor;
    public Color barrierColor;
    public Color mainCylinderColor;

    public LevelColor(Color backgroundColor, Color targetColor, Color barrierColor, Color mainCylinderColor)
    {
        this.backgroundColor = backgroundColor;
        this.targetColor = targetColor;
        this.barrierColor = barrierColor;
        this.mainCylinderColor = mainCylinderColor;
    }
}

public class GameColor : MonoBehaviour
{
    public List<LevelColor> levelColors = new List<LevelColor>();
    LevelColor levelColor;
    int colorIndex;

    private void Awake()
    {
        levelColors.Add(new LevelColor(
            new Color(0.5019607f, 0.6470588f, 0.1254901f),
            new Color(1, 0.2705882f, 0),
            new Color(0.9333333f, 0.5098039f, 0.9333333f),
            new Color(0, 0.5019607f, 0.5019607f))
            );

        levelColors.Add(new LevelColor(
            new Color(1, 0.6274509f, 0.4784313f),
            new Color(0.7803921f, 0.0823529f, 0.5215686f),
            new Color(0, 0, 1),
            new Color(0.6039215f, 0.8039215f, 0.1960784f))
            );

        levelColors.Add(new LevelColor(
            new Color(0.5411764f, 0.1686274f, 0.8862745f),
            new Color(0, 0.5019607f, 0),
            new Color(0.7215686f, 0.5254901f, 0.0431372f),
            new Color(0.6980392f, 0.1333333f, 0.1333333f))
            );

        colorIndex = levelColors.Count;
        RandomLevelColor();
    }

    private void RandomLevelColor()
    {
        int levelIndex = UnityEngine.Random.Range(0, levelColors.Count);
        if (levelIndex != colorIndex)
            colorIndex = levelIndex;
        levelColor = levelColors[colorIndex];
    }

    public void NextLevel()
    {
        RandomLevelColor();
    }

    public Color GetMainColor()
    {
        return levelColor.mainCylinderColor;
    }

    public Color GetBackgroundColor()
    {
        return levelColor.backgroundColor;
    }

    public Color GetTargetColor()
    {
        return levelColor.targetColor;
    }

    public Color GetBarrierColor()
    {
        return levelColor.barrierColor;
    }
}

public enum Colors
{
    Yellow,
    YellowOrange,
    Orange,
    RedOrange,
    Red,
    RedViolet,
    Violet,
    BlueViolet,
    Blue,
    BlueGreen,
    Green,
    YellowGreen
}
