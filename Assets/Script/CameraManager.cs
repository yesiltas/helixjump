﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    public Player player;

    public float distance;
    public float height;

    public void StartGame()
    {
        transform.position = transform.parent.position;
        transform.position -= Vector3.forward * distance;
        transform.position += Vector3.up * height;
    }
}
