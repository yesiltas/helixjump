﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    public Action<bool> GameFinish;
    public Action PlayerScore;
    public Player player;
    public CylinderManager cylinderManager;
    public CameraManager cameraManager;
    public CameraFollowManager followManager;
    public GameColor gameColor;
    bool isNextLevel;
    int level = 0;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        player.PlayerDie += PlayerDie;
        player.PlayerWin += Playerwin;
        player.PlayerPassedBarrier += PlayerPassedBarrier;
    }

    public void StartGame()
    {
        if (level % 5 == 0 && level != 0)
            cylinderManager.StartGame(true);
        else
            cylinderManager.StartGame(false);
        Camera.main.backgroundColor = GetBackgroundColor();
        player.gameObject.SetActive(true);
        player.StartGame();
        followManager.StartGame();
        cameraManager.StartGame();
    }

    public void Again()
    {
        player.gameObject.SetActive(false);
        player.Again();
        cylinderManager.Again(isNextLevel);
        if (isNextLevel)
        {
            gameColor.NextLevel();
            level++;
        }
    }

    private void Playerwin()
    {
        isNextLevel = true;
        GameFinish(true);
        cylinderManager.FinishGame();
    }

    private void PlayerDie()
    {
        isNextLevel = false;
        GameFinish(false);
        cylinderManager.FinishGame();
    }

    private void PlayerPassedBarrier(CylinderController cylinder, bool isEmpowered)
    {
        StartCoroutine(ExecuteAfterTime(0.2f, cylinder));
        cylinder.Destroy();
        followManager.SetTargetPosition(isEmpowered);
        PlayerScore();
    }

    IEnumerator ExecuteAfterTime(float time, CylinderController cylinder)
    {
        yield return new WaitForSeconds(time);
        Destroy(cylinder.gameObject);
        cylinderManager.RemoveInList(cylinder);
    }

    public Vector3 GetPosition()
    {
        return cylinderManager.GetPosition();
    }

    public Color GetMainColor()
    {
        return gameColor.GetMainColor();
    }

    public Color GetBackgroundColor()
    {
        return gameColor.GetBackgroundColor();
    }

    public Color GetTargetColor()
    {
        return gameColor.GetTargetColor();
    }

    public Color GetBarrierColor()
    {
        return gameColor.GetBarrierColor();
    }

}
