﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CylinderController : MonoBehaviour
{
    public GameObject slice;
    public List<Renderer> slices = new List<Renderer>();
    public List<SliceController> sliceGroup = new List<SliceController>();
    int ID;
    Color barrierColor;
    Color targetColor;
    public GameObject prefab;

    private void Awake()
    {
        barrierColor = GameManager.instance.GetBarrierColor();
        targetColor = GameManager.instance.GetTargetColor();
    }

    public void Initialize(Vector3 position, List<int> space, List<int> barrier,bool isChangeableBarrier)
    {
        transform.rotation = Quaternion.Euler(0, 0, 0);
        for (int i = 0; i < slices.Count; i++)
        {
            if (space.Contains(i))
            {
                slices[i].GetComponent<MeshRenderer>().enabled = false;
                slices[i].gameObject.tag = "space";
                slices[i].GetComponent<MeshCollider>().convex = true;
                slices[i].GetComponent<MeshCollider>().isTrigger = true;
                slices[i].transform.localScale = new Vector3(1, 0.1f, 1);
            }
            else if (barrier.Contains(i))
            {
                slices[i].material.SetColor("_Color", barrierColor);
                slices[i].gameObject.tag = "barrier";
            }
            else
            {
                slices[i].material.SetColor("_Color", targetColor);
                slices[i].gameObject.tag = "target";
            }
        }

        transform.position = position;

        if (isChangeableBarrier)
        {
            ChangeableBarrierManager changeableBarrier = Instantiate(prefab).GetComponent<ChangeableBarrierManager>();
            changeableBarrier.Initialize(barrierColor);
            changeableBarrier.transform.position = position;
            changeableBarrier.transform.parent = this.transform;
            sliceGroup.Add(changeableBarrier.GetComponent<SliceController>());
        }

    }

    public void Destroy()
    {
        foreach (var item in sliceGroup)
        {
            item.Destroy();
        }
    }

    public void DisenableSlices()
    {
        foreach (var item in slices)
        {
            Destroy(item.gameObject);
        }
    }

    public void SetID(int ID)
    {
        this.ID = ID;
    }

    public int GetID()
    {
        return ID;
    }
}