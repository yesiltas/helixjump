﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject startPanel;
    public GameObject gamePanel;
    public GameObject finishPanel;
    public Text startText;
    public Text finishText;
    public Text winloseText;
    public Text scoreText;
    public GameManager gameManager;

    int score;

    private void Awake()
    {
        startText.text = "Tap To Start";       
    }

    private void Start()
    {
        gameManager.GameFinish += OnGameFinished;
        gameManager.PlayerScore += PlayerScore;
    }

    public void StartGame()
    {
        gameManager.StartGame();
        score = -1;
        SetScore();
        OpenGamePanel();
    }

    public void Again()
    {
        gameManager.Again();
        OpenStartPanel();
    }

    private void OnGameFinished(bool isPlayerWin)
    {
        if (isPlayerWin)
        {
            finishText.text = "Next Level";
            winloseText.text = "You Win";
        }
        else
        {
            finishText.text = "Play Again";
            winloseText.text = "You Lose";
        }
        OpenFinishPanel();
    }

    private void PlayerScore()
    {
        SetScore();
    }

    private void OpenFinishPanel()
    {
        startPanel.SetActive(false);
        gamePanel.SetActive(false);
        finishPanel.SetActive(true);
    }

    private void OpenGamePanel()
    {
        startPanel.SetActive(false);
        gamePanel.SetActive(true);
        finishPanel.SetActive(false);
    }

    private void OpenStartPanel()
    {
        startPanel.SetActive(true);
        gamePanel.SetActive(false);
        finishPanel.SetActive(false);
    }

    private void SetScore()
    {
        score++;
        scoreText.text = "Score: " + score;
    }
}
