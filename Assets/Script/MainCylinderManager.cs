﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCylinderManager : MonoBehaviour
{
    List<CylinderController> cylinders = new List<CylinderController>();
    public CylinderController finishCylinder;
    Renderer rnd;

    public float sensetive;
    float startPosition;
    float currentPosition;
    bool isGamePlaying = false;

    Color main;
    Color targetColor;

    private void Awake()
    {
        rnd = GetComponent<Renderer>();
        main = GameManager.instance.GetMainColor();
        targetColor = GameManager.instance.GetTargetColor();
    }

    public void StartGame()
    {
        StartCoroutine(ExecuteAfterTime(0.1f));
    }

    public void FinishGame()
    {
        isGamePlaying = false;
    }

    public void Initialize(int cylinderCount)
    {
        rnd.material.SetColor("_Color", main);
        for (int i = 0; i < finishCylinder.transform.childCount; i++)
        {
            finishCylinder.transform.GetChild(i).GetComponent<Renderer>().material.SetColor("_Color", targetColor);
        }
        transform.localScale += new Vector3(0, cylinderCount, 0);
        transform.rotation = Quaternion.Euler(0, 0, 0);       
    }

    public void GetChildCylinder(CylinderController cylinderController)
    {
        cylinderController.transform.parent = this.transform;
        cylinders.Add(cylinderController);
        Vector3 scale =cylinderController.transform.localScale;
        finishCylinder.transform.localScale = scale;
    }

    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        isGamePlaying = true;
    }

    void Update()
    {
        if (isGamePlaying)
        {
            if (Input.GetMouseButtonDown(0))
                startPosition = Input.mousePosition.x;
            if (Input.GetMouseButton(0))
            {
                currentPosition = Input.mousePosition.x;
                Swipe();
            }
        }
    }

    private void Swipe()
    {
        float distance;
        float offset;
        distance = (currentPosition - startPosition) * 265 / Screen.width * 0.75f;
        offset = distance * sensetive * -1;
        transform.rotation = Quaternion.Euler(0, offset + transform.rotation.eulerAngles.y, 0);
        startPosition = currentPosition;
    }

    public void DestroyCylinders()
    {
        foreach (var item in cylinders)
        {
            Destroy(item.gameObject);
        }
        cylinders.Clear();
    }

    public void RemoveInList(CylinderController cylinderController)
    {
        cylinders.Remove(cylinderController);
    }
}
