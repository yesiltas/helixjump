﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SliceController : MonoBehaviour
{
    bool isDestroy = false;
    float speed = 15f;

    public void Destroy()
    {
        isDestroy = true;
    }

    void FixedUpdate()
    {
        if (isDestroy)
        {
            transform.position += -transform.right * Time.deltaTime * speed;
        }
    }
}
