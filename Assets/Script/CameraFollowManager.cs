﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowManager : MonoBehaviour
{
    public float speed;
    Vector3 targetPosition;
    Vector3 startPosition;

    public void StartGame()
    {
        startPosition = GameManager.instance.GetPosition();
        transform.position = startPosition;
        targetPosition.y = startPosition.y + 4;
    }

    public void SetTargetPosition(bool isEmpowered)
    {
        if (isEmpowered)
            StartCoroutine(ExecuteAfterTime(1));
        else
            StartCoroutine(ExecuteAfterTime(0));
    }

    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        targetPosition = new Vector3(startPosition.x, startPosition.y - 4, startPosition.z);
        startPosition = targetPosition;
    }

    void FixedUpdate()
    {
        if (transform.position.y > targetPosition.y)
            transform.Translate(Vector3.down * speed * Time.deltaTime);
    }
}
